package response

type ErrorResponse struct {
	Error ErrorResponseError `json:"error"`
}

type ErrorResponseError struct {
	Code        int    `json:"code"`
	MessageCode string `json:"messageCode"`
	Message     string `json:"message"`
}
