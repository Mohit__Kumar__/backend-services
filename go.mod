module github.com/backend-services

go 1.14

require (
	github.com/gorilla/mux v1.7.4
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.7.0
	github.com/sirupsen/logrus v1.6.0
)
