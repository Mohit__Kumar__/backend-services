package dataservices

import (
	"database/sql"
)

// PostgresClient stored reference to the DB
type PostgresClient struct {
	DB          *sql.DB
	DBRadiuzDWH *sql.DB
	DBYor24DWH  *sql.DB
}

// IPostgresClient - DB interface
type IPostgresClient interface {
	Connect()
	// ----------------->
}
